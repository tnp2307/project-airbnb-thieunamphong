import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import Layout from "./Layout/Layout";
import Homepage from "./Page/Homepage/Homepage";

import DetailPage from "./Page/DetailPage/DetailPage";
import { Component } from "react";

import Location from "./Page/Location/Location";
import LoginAndSignupModal from "./Page/Homepage/LoginAndSignupModal";
import NotFoundLocationPage from "./Page/Homepage/NotFoundPage/NotFoundLocationPage";
import { adminRoute } from "./Route/Route";
import Spinner from "./Component/Spinner/Spinner";
import UserPage from "./Page/UserPage/UserPage/UserPage";

// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyC6LJV2oKvByhT7MCqcNgoySql8Qeemv8o",
  authDomain: "vie-homestay.firebaseapp.com",
  projectId: "vie-homestay",
  storageBucket: "vie-homestay.appspot.com",
  messagingSenderId: "930856592498",
  appId: "1:930856592498:web:090df0af660261724c4eeb",
  measurementId: "G-TMYQHZ7WZQ"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

function App() {
  return (
    <div>
      <Spinner />
      <LoginAndSignupModal />
      <BrowserRouter>
        <Routes>
          <Route path="/homepage" element={<Layout Component={Homepage} />} />
          <Route
            path="/location/:id"
            element={<Layout Component={Location} />}
          />
          <Route
            path="notfound"
            element={<Layout Component={NotFoundLocationPage} />}
          />
          <Route
            path="*"
            element={<Layout Component={NotFoundLocationPage} />}
          />
          <Route
            path="/detail/:id"
            element={<Layout Component={DetailPage} />}
          />
          <Route
            path="/user-information"
            element={<Layout Component={UserPage} />}
          />

          {adminRoute.map(({ url, component }) => {
            return <Route key={url} path={url} element={component} />;
          })}
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
